<?php

namespace Modules\Cambodium\Http\Controllers\Backend;

use App\Authorizable;
use App\Http\Controllers\Backend\BackendBaseController;

class CambodiaController extends BackendBaseController
{
    use Authorizable;

    public function __construct()
    {
        // Page Title
        $this->module_title = 'Cambodia';

        // module name
        $this->module_name = 'cambodia';

        // directory path of the module
        $this->module_path = 'cambodium::backend';

        // module icon
        $this->module_icon = 'fa-regular fa-sun';

        // module model name, path
        $this->module_model = "Modules\Cambodium\Models\Cambodium";
    }

}
