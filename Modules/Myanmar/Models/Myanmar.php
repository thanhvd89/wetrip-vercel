<?php

namespace Modules\Myanmar\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Myanmar extends BaseModel
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'myanmars';

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory()
    {
        return \Modules\Myanmar\database\factories\MyanmarFactory::new();
    }
}
