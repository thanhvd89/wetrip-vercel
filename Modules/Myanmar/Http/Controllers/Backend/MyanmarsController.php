<?php

namespace Modules\Myanmar\Http\Controllers\Backend;

use App\Authorizable;
use App\Http\Controllers\Backend\BackendBaseController;

class MyanmarsController extends BackendBaseController
{
    use Authorizable;

    public function __construct()
    {
        // Page Title
        $this->module_title = 'Myanmars';

        // module name
        $this->module_name = 'myanmars';

        // directory path of the module
        $this->module_path = 'myanmar::backend';

        // module icon
        $this->module_icon = 'fa-regular fa-sun';

        // module model name, path
        $this->module_model = "Modules\Myanmar\Models\Myanmar";
    }

}
