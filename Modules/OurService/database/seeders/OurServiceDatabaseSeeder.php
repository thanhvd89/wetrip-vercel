<?php

namespace Modules\OurService\database\seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Tag\Models\OurService;

class OurServiceDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Disable foreign key checks!
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        /*
         * OurServices Seed
         * ------------------
         */

        // DB::table('ourservices')->truncate();
        // echo "Truncate: ourservices \n";

        OurService::factory()->count(20)->create();
        $rows = OurService::all();
        echo " Insert: ourservices \n\n";

        // Enable foreign key checks!
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
