@extends('frontend.layouts.app')

@section('title') {{ __($module_title) }} @endsection

@section('content')

<div class="our-services">
    <section class="our-services__info container p-md-5 p-3">
        <div class="d-flex flex-column justify-content-center align-items-center mb-3">
            <p class="wt-font-merriweather wt-fw-300 fst-italic wt-color-gray">Wetrip Travel</p>
            <h4 class="wt-font-antonio wt-fw-400 wt-fz-48 wt-color-blue my-4">Our Services</h4>
        </div>
        <div class="d-flex flex-column align-items-center justify-content-center">
            <div class="our-services__info--description d-flex flex-column justify-content-center align-items-center">
                <i class="fa-solid fa-quote-left wt-color-blue-2 wt-fz-26 mb-3 opacity-25"></i>
                <p class="wt-color-blue fst-italic wt-fw-400 wt-fz-18 text-center mb-2">We cater wide selections of travel services including Visa with VIP Access, airport transfer; air tickets, hotel and cruise bookings, short excursions; Beach holidays; and door-to-door packages across Vietnam, Cambodia, Laos and Myanmar.</p>
                <p class="wt-color-blue fst-italic wt-fw-400 wt-fz-18 text-center ">Our guests are very various, from single traveler, honeymoon couple, families to ad hoc groups of leisure, set departures, or large scale of MICE groups with hundreds of people traveling in the same time.</p>
            </div>
            <div class="d-flex justify-content-center w-100 mt-5">
                <div class="w-50 position-relative wt-line-gradient"></div>
            </div>
        </div>
    </section>
    <section class="our-services__content container p-md-5 p-3">
        <div class="our-services__content--mice mb-5 pb-5">
            <div class="row d-flex align-items-center">
                <div class="col-lg-8 col-md-6 col-12 mb-3">
                    <h5 class="wt-font-merriweather fw-bold fst-italic wt-fz-36 mb-4">MICE</h5>
                    <p class="wt-color-gray wt-fw-400 wt-fz-16 lh-lg">Emerging as one of the most searching destinations in South East Asia for MICE segment, Vietnam becomes an attractive place to go for many MICE groups thanks to the redundancy that a destination can offer from breathtaking landscape, wide range of accommodation from local to the international top luxury brands, big capacity of the restaurants, excited activities, nightlife and so on.
                        <br>
                       Whatever the purpose of your trip is, company reward incentive trip or business conference/cooperate event, Wetripasia is committed to offering one stop solution for all MICE requirements and creating an experience-rich trip.
                    </p>
                </div>
                <div class="col-lg-4 col-md-6 col-12">
                    <img class="w-100" src="{{asset('img/image 1 (1).png')}}" alt="image 1 (1)">
                </div>
            </div>
        </div>
        <div class="our-services__content--tour mb-5 pb-3">
            <div class="row d-flex align-items-center">
                <div class="col-lg-4 col-12 mb-3">
                    <img class="w-100" src="{{asset('img/image 1 (1).png')}}" alt="image 1 (1)">
                </div>
                <div class="col-lg-8 col-12">
                    <h5 class="wt-font-merriweather fw-bold fst-italic wt-fz-36 mb-4">Active Tours</h5>
                    <p class="wt-color-gray wt-fw-400 wt-fz-16 lh-lg">
                        If you are a nature lover and seeking for series of outdoor activities, Vietnam will be a destination of all-in-one for many types of activities from trekking thru the off-beaten trace and hill tribe villages in the northern part to biking on a dirt road of mountainous areas and along the coastal line from north to south; paragliding over the golden terrace rice fields from a hill top or challenging your limitation by rock climbing; kitesurfing under the clear blue sky at a certain central beach or scuba diving under the crystal water contemplating the colorful coral reefs; gentle rafting on mountainous river or kayak on a turquoise water in Halong Bay; being thrill with ziplines game and high rope course in the jungle having bird eye view of the landscape or venturing on a motorbike riding into the labyrinth of alleys and backstreets catching a glimpse of the hidden gems and authentic lifestyle of people in the big cities or countryside scenery,.. Let our active team help you plan a busy schedule of adventure with full of joy and fun.
                    </p>
                </div>
            </div>
        </div>
        <div class="our-services__content--theme mb-5">
            <div class="row">
                <div class="col-lg-8 col-12">
                    <h3 class="wt-font-merriweather fw-bold fst-italic wt-fz-36 mb-4">Theme Tours</h3>
                    <div class="mb-4">
                        <h5 class="wt-font-merriweather fw-bold fst-italic wt-fz-20 mb-4">
                            Wellness Tour
                        </h5>
                        <p class="wt-color-gray wt-fw-400 wt-fz-16 lh-lg">
                            Emerging as one of the most searching destinations in South East Asia for MICE segment, Vietnam becomes an attractive place to go for many MICE groups thanks to the redundancy that a destination can offer from breathtaking landscape, wide range of accommodation from local to the international top luxury brands, big capacity of the restaurants, excited activities, nightlife and so on.
                            <br>
                            Whatever the purpose of your trip is, company reward incentive trip or business conference/cooperate event, Wetripasia is committed to offering one stop solution for all MICE requirements and creating an experience-rich trip.
                        </p>
                    </div>
                    <div class="mb-4">
                        <h5 class="wt-font-merriweather fw-bold fst-italic wt-fz-20 mb-4">
                            Education Tour
                        </h5>
                        <p class="wt-color-gray wt-fw-400 wt-fz-16 lh-lg">
                            As a country with rich history, variety of cultural and geographical attractions, dozens of national parks with diverse flora and fauna, vibrant pace of life featuring of a developing country in Asia, Vietnam is an ideal destination for many education groups to come experience and study. Whether they are university students in majors of architecture, history, fine arts, biology,.. who want to have practical research and studies in their fields or are just high school students in a responsible trip supporting to local communities, we know how to design an authentic experiential trip, providing bunches of gainful insights.
                        </p>
                    </div>
                    <div class="mb-4">
                        <h5 class="wt-font-merriweather fw-bold fst-italic wt-fz-20 mb-4">
                            Kitchen Access Tours
                        </h5>
                        <p class="wt-color-gray wt-fw-400 wt-fz-16 lh-lg">
                            Despite of the availability of multiple Indian restaurants across the country, many groups still prefer having their own escort chef to cook for their meal as the signature of the trip. Wetripasia can perfectly handle this request without any hassle. Having strong partnership and relationship with the local suppliers supports us arranging the kitchen access and catering all requirements of raw material to cook fluently. Your chef is assured to be received the greatest support and help from local teams to fulfill his job
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-12">
                    <div class="tour-images d-flex flex-column justify-content-between">
                        <div class="image-1 active position-relative wt-cursor">
                            <img class="w-100 h-100" src="{{asset('img/BG.png')}}" alt="BG">
                            <h6 class="position-absolute text-white wt-fw-600 wt-fz-24">Tour 1</h6>
                        </div>
                        <div class="image-2 position-relative wt-cursor">
                            <img class="w-100 h-100" src="{{asset('img/BG.png')}}" alt="BG">
                            <h6 class="position-absolute text-white wt-fw-600 wt-fz-24">Tour 2</h6>
                        </div>
                        <div class="image-3 position-relative wt-cursor">
                            <img class="w-100 h-100" src="{{asset('img/BG.png')}}" alt="BG">
                            <h6 class="position-absolute text-white wt-fw-600 wt-fz-24">Tour 3</h6>
                        </div>
                        <div class="image-4 position-relative wt-cursor">
                            <img class="w-100 h-100" src="{{asset('img/BG.png')}}" alt="BG">
                            <h6 class="position-absolute text-white wt-fw-600 wt-fz-24">Tour 4</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection
