<?php

namespace Modules\OurService\Console\Commands;

use Illuminate\Console\Command;

class OurServiceCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:OurServiceCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'OurService Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        return Command::SUCCESS;
    }
}
