<?php

namespace Modules\OurService\Http\Controllers\Backend;

use App\Authorizable;
use App\Http\Controllers\Backend\BackendBaseController;

class OurServicesController extends BackendBaseController
{
    use Authorizable;

    public function __construct()
    {
        // Page Title
        $this->module_title = 'OurServices';

        // module name
        $this->module_name = 'ourservices';

        // directory path of the module
        $this->module_path = 'ourservice::backend';

        // module icon
        $this->module_icon = 'fa-regular fa-sun';

        // module model name, path
        $this->module_model = "Modules\OurService\Models\OurService";
    }

}
