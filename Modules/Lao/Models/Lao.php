<?php

namespace Modules\Lao\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lao extends BaseModel
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'laos';

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory()
    {
        return \Modules\Lao\database\factories\LaoFactory::new();
    }
}
