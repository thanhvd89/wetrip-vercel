<?php

namespace Modules\Lao\Console\Commands;

use Illuminate\Console\Command;

class LaoCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:LaoCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lao Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        return Command::SUCCESS;
    }
}
