<?php

namespace Modules\Vietnam\database\seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Tag\Models\Vietnam;

class VietnamDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Disable foreign key checks!
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        /*
         * Vietnams Seed
         * ------------------
         */

        // DB::table('vietnams')->truncate();
        // echo "Truncate: vietnams \n";

        Vietnam::factory()->count(20)->create();
        $rows = Vietnam::all();
        echo " Insert: vietnams \n\n";

        // Enable foreign key checks!
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
