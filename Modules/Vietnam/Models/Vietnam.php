<?php

namespace Modules\Vietnam\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vietnam extends BaseModel
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'vietnams';

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory()
    {
        return \Modules\Vietnam\database\factories\VietnamFactory::new();
    }
}
